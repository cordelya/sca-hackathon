# SCA Hack-a-thons

What if there was an annual virtual or semi-virtual event focused on putting members' technological skills to work for the purpose of improving or enhancing SCA operations, experiences, and administration?

- Such an event should be open to all participants, regardless of skill level.
- There is always a need for QA testing and documentation writing that does not require actual writing of code
- This could be a way for those interested in learning to get their feet wet

How do we facilitate collaboration and respository sharing?

Possibilities:
- Github Organization 
  - Free tier available
  - Add members and grant access to specific repositories
  - All projects collected under one "account"
  - All participants would need to have an account on that host, or at minimum, deploy keys assigned
- A single project that collects links to individual project repositories, regardless of where they are hosted
  - Allows individuals doing sole work to retain ownership of their work
  - Allows team leads to self-manage who gets write access and who must work via a fork
  - Allows individuals to use the repository host of their choice
  - Anyone who might be a manager of that summary repository needs to have an account on the host

Comms and prep:
- Use a dedicated closed mailing list to communicate information to participants - videoconference links, etc.
- Use a Google Form to do an interest inventory as soon as possible (how many people are interested? what skill levels will we have? etc)
- Use a Google Form to collect sign-ups, including repository host account usernames to facilitate setting up access and assigning people to teams when they aren't forming a team on their own.
- Reach out to those who are maintaining applications already in use, such as Thing, to see if they would like to run a team focused on improving or enhancing those apps. Facilitate pre-meetings to onboard new contributors and help them get a working environment up.
- Specify testing, code standards checking, and inline documentation standards for new applications and provide assistance to those who are unfamiliar with such things.
- Schedule a round-table in advance to talk about tools and environments for those who want to learn.
- Schedule some training in advance for git and git remote repository ops. Set up a training repository to allow class attendees a repository to work with while they learn.

Facets:
- This event can be youth-friendly. Any youth who want to participate but are too young for regular programming can be invited to make and submit Scratch or Scratch Jr. sketches, and links to those can be included in the list of projects.
- What any one person produces doesn't need to be an entire application. Even a small script that makes a single task easier is sufficient.
  - Example: This Python script which [produces 12 months of email message content](https://gitlab.com/cordelya/python-minis/-/blob/0dd4eb8551b5e9884a8f5c1f7f766f40af643cda/oh-mailmerge.py) intended for generating 12 months' worth of monthly meeting reminder emails with Agenda deadlines + when the emails should be sent. Plug in the details, save & run the script (`python3 oh-mailmerge.py`), and use the resulting text file to copy/paste the Subject and Body for each meeting into your email composer one email at a time, then schedule each to be sent out on the date indicated in the file. Now all of your meeting emails are written uniformly and you don't have to remember to send them.
